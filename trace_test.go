package errors_test

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"testing"

	"gitlab.com/seagulls/go-errors"
)

func foo() error {
	return fmt.Errorf("oops")
}

func bar() error {
	if err := foo(); err != nil {
		return errors.Wrap(err, errors.OptTrace(1))
	}

	return nil
}

func TestTrace_Error(t *testing.T) {
	err := bar()
	actual := err.Error()
	expects := `err(oops) {"pkg":"gitlab.com/seagulls/go-errors_test","fn":"bar","file":"%s","line":"19"}`
	wd, _ := os.Getwd()
	p := filepath.ToSlash(path.Join(wd, "trace_test.go"))
	expects = fmt.Sprintf(expects, p)

	if actual != expects {
		t.Fatalf("expected %s, got %s", expects, actual)
	}
}

func TestTrace_Error_GoRoutine(t *testing.T) {
	expects := `err(oops) {"pkg":"gitlab.com/seagulls/go-errors_test","fn":"bar","file":"%s","line":"19"}`
	wd, _ := os.Getwd()
	p := filepath.ToSlash(path.Join(wd, "trace_test.go"))
	expects = fmt.Sprintf(expects, p)

	errCh := make(chan error, 1)
	defer close(errCh)
	go func() {
		errCh <- bar()
	}()
	err := <-errCh
	actual := err.Error()

	if actual != expects {
		t.Fatalf("expected %s, got %s", expects, actual)
	}
}
