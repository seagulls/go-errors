package errors

import (
	"encoding/json"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

const (
	pkg   = 1
	fn    = 2
	fpath = 1
	line  = 2
)

var (
	pRgx = regexp.MustCompile(`(?m)^([^(]+)\.(\w+)\((?:0x[0-9a-f]+\W*)*\)$`)
	fRgx = regexp.MustCompile(`(?m)^\t(.+):(\d+)\W\+0x[0-9a-f]+$`)
)

type trace struct {
	Pkg  string `json:"pkg"`
	Fn   string `json:"fn"`
	File string `json:"file"`
	Line string `json:"line"`
}

func (t trace) String() string {
	raw, err := json.Marshal(t)
	if err != nil {
		return `{"error":"could not marshal trace"}`
	}
	return string(raw)
}

func OptTrace(depth int) Option {
	t := trace{}

	var buf = make([]byte, 512)
	_ = runtime.Stack(buf, false)

	var t0, t1 int
	lines := strings.Split(string(buf), "\n")
	for i, l := range lines {
		if m := pRgx.FindStringSubmatch(l); m != nil {
			if t0 == depth {
				t.Pkg = m[pkg]
				t.Fn = m[fn]
				t1 = i + 1
				break
			} else {
				t0++
				continue
			}
		}
	}
	if m := fRgx.FindStringSubmatch(lines[t1]); m != nil {
		t.File = filepath.ToSlash(m[fpath])
		t.Line = m[line]
	}

	return func(e error) error {
		et, ok := e.(err)
		if !ok {
			return e
		}

		et[KeyTrace] = t
		return et
	}
}
