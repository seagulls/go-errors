package errors

const (
	Timeout = Const(`timeout`)
	EOF     = Const(`eof`)
)

type Const string

func (e Const) Error() string {
	return string(e)
}
