package errors

import (
	"bytes"
	"fmt"
)

type Attr struct {
	K string
	V any
}

func (a Attr) String() string {
	return fmt.Sprintf("%q:%s", a.K, maybeQuote(a.V))
}

func maybeQuote(arg any) string {
	switch v := arg.(type) {
	case int, int8, int16, int32, int64,
		uint8, uint16, uint32, uint64,
		float32, float64:
		return fmt.Sprint(v)
	}

	return fmt.Sprintf("%q", arg)
}

type Attrs []Attr

func (a Attrs) String() string {
	buf := bytes.Buffer{}
	buf.WriteString(`{`)

	for i, v := range a {
		if i > 0 {
			buf.WriteString(`,`)
		}
		buf.WriteString(v.String())
	}
	buf.WriteString(`}`)

	return buf.String()
}

func Attribute(e error, args ...any) error {
	return &err{
		KeyErr:  e,
		KeyData: pack(args...),
	}
}

func OptAttr(args ...any) Option {
	d := pack(args...)

	return func(e error) error {
		et, ok := e.(err)
		if !ok {
			return e
		}

		et[KeyData] = d
		return et
	}
}

func pack(args ...any) Attrs {
	d := make(Attrs, 0, len(args)/2)

	for idx := 1; idx < len(args); idx += 2 {
		k, ok := args[idx-1].(string)
		if !ok {
			continue
		}
		d = append(d, Attr{K: k, V: args[idx]})
	}

	return d
}
