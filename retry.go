package errors

import (
	"context"
	"time"
)

func Retry(ctx context.Context, fn func() error, wait time.Duration) error {
	var errs MultiError

	for {
		// Run the function.
		err := fn()
		if err == nil {
			// Success, so return nil.
			return nil
		}
		// Append the latest error.
		errs = append(errs, err)

		select {
		case <-ctx.Done():
			// Context done, record a Timeout error and return.
			errs := append(errs, Timeout)
			return errs
		case <-time.After(wait):
			// try again
		}
	}
}
