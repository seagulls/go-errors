package errors_test

import (
	"os"
	"strings"
	"testing"

	qt "github.com/frankban/quicktest"

	"gitlab.com/seagulls/go-errors"
)

const (
	fixOops = errors.Const(`oops`)

	tplPath = `__PATH__`
)

func TestErrors(t *testing.T) {
	c := qt.New(t)
	wd, _ := os.Getwd()

	subtest := func(e error, expect string) func(*qt.C) {
		return func(c *qt.C) {
			expect = strings.ReplaceAll(expect, tplPath, wd)

			actual := e.Error()
			c.Assert(actual, qt.Equals, expect)
		}
	}

	c.Run(`simple error`, subtest(errors.New(`foo`), `foo`))

	c.Run(`simple error + description`, subtest(errors.Wrap(fixOops, errors.OptDescribe(`foo`)), `foo err(oops)`))
	c.Run(`simple error + attrs`, subtest(errors.Wrap(fixOops, errors.OptAttr(`bar`, `baz`)), `err(oops) {"bar":"baz"}`))
	c.Run(`simple error + trace`, subtest(
		errors.Wrap(fixOops, errors.OptTrace(1)),
		`err(oops) {"pkg":"gitlab.com/seagulls/go-errors_test","fn":"TestErrors","file":"__PATH__/error_test.go","line":"37"}`,
	))

	c.Run(`simple error + description + attrs`, subtest(errors.Wrap(fixOops, errors.OptDescribe(`foo`), errors.OptAttr(`bar`, `baz`)), `foo err(oops) {"bar":"baz"}`))
	c.Run(`simple error + all`, subtest(
		errors.Wrap(
			fixOops,
			errors.OptDescribe(`foo`),
			errors.OptAttr(`bar`, `baz`),
			errors.OptTrace(1),
		),
		`foo err(oops) {"bar":"baz"} {"pkg":"gitlab.com/seagulls/go-errors_test","fn":"TestErrors","file":"__PATH__/error_test.go","line":"47"}`,
	))
}

func TestErrors_InjectConst(t *testing.T) {
	c := qt.New(t)
	err := errors.New(`foo`, errors.OptDescribe(`bar`))

	expect := `bar err(foo)`
	actual := err.Error()
	c.Assert(actual, qt.Equals, expect)

	err = errors.New(`foo`, errors.OptAttr(`bar`, 12.34), errors.OptDescribe(`dropped it`), errors.OptTrace(1))
	err = errors.Wrap(err, errors.OptDescribe(`something went wrong`))

	expect = `something went wrong err(dropped it err(foo) {"bar":12.34} {"pkg":"gitlab.com/seagulls/go-errors_test","fn":"TestErrors_InjectConst","file":"/home/seagles/go/src/gitlab.com/seagulls/go-errors/error_test.go","line":"61"})`
	actual = err.Error()
	c.Assert(actual, qt.Equals, expect)
}
