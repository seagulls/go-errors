package errors

import "strings"

type injectable interface {
	inject(error) bool
}

// Option is a mutator for errors provided by this package.
type Option func(error) error

type MultiError []error

func (m MultiError) Error() string {
	switch len(m) {
	case 0:
		return ""
	case 1:
		return m[0].Error()
	}

	var b strings.Builder
	b.WriteString(m[0].Error())
	for _, s := range m[1:] {
		b.WriteString("\n")
		b.WriteString(s.Error())
	}
	return b.String()
}

// Wrap returns an error wrapped with extra information provided
// by the opts passed in.
func Wrap(e error, opts ...Option) error {
	if len(opts) == 0 {
		// cover the original proxied function
		return e
	}

	// Wrap the error.
	e = err{
		KeyErr: e,
	}

	for _, opt := range opts {
		e = opt(e)
	}
	return e
}

// HasRoot unwraps the error until it hits a Const type. The function
// returns true if the Const found is e.
func HasRoot(err error, e Const) bool {
	var actual Const
	if !As(err, &actual) {
		return false
	}

	return actual == e
}

func Assert(err error) error {
	if err == error(nil) {
		return nil
	}

	if e := err.(MultiError); len(e) == 0 {
		return nil
	}

	return err
}
