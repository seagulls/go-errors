package errors

import "bytes"

type ErrAttr string

const (
	KeyErr   = ErrAttr(`error`)
	KeyDesc  = ErrAttr(`desc`)
	KeyData  = ErrAttr(`data`)
	KeyTrace = ErrAttr(`trace`)
)

type err map[ErrAttr]any

func (e err) Error() string {
	var l int
	buf := &bytes.Buffer{}

	if v, ok := e[KeyDesc]; ok {
		n, _ := buf.WriteString(v.(string))
		l += n
	}

	if v, ok := e[KeyErr]; ok {
		if l > 0 {
			buf.WriteString(` `)
		}
		buf.WriteString(`err(`)
		n, _ := buf.WriteString(v.(error).Error())
		buf.WriteString(`)`)
		l += n + 2
	}

	if v, ok := e[KeyData]; ok {
		if l > 0 {
			buf.WriteString(` `)
		}
		n, _ := buf.WriteString(v.(Attrs).String())
		l += n
	}

	if v, ok := e[KeyTrace]; ok {
		if l > 0 {
			buf.WriteString(` `)
		}
		buf.WriteString(v.(trace).String())
	}

	return buf.String()
}

func (e err) Unwrap() error {
	if v, ok := e[KeyErr]; ok {
		return v.(error)
	}

	return nil
}

func (e err) inject(err error) bool {
	v, ok := e[KeyErr]
	if !ok {
		e[KeyErr] = err
		return true
	}

	if target, ok := v.(injectable); ok {
		return target.inject(err)
	}

	return false
}
