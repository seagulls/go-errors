package errors_test

import (
	"context"
	"testing"
	"time"

	qt "github.com/frankban/quicktest"

	"gitlab.com/seagulls/go-errors"
)

const (
	errFixture = errors.Const(`nope`)
)

func TestRetry(t *testing.T) {
	c := qt.New(t)

	subtest := func(ttl, wait, fix time.Duration, expect string) func(*qt.C) {
		return func(c *qt.C) {
			ctx, cancel := context.WithTimeout(context.Background(), ttl)
			defer cancel()

			err := errors.Retry(ctx, fixture(fix), wait)

			if len(expect) == 0 {
				c.Assert(err, qt.IsNil)
			} else {
				c.Assert(err, qt.ErrorMatches, expect)
			}
		}
	}

	c.Run(`ok - succeed first try`, subtest(time.Second, 10*time.Millisecond, 0, ``))
	c.Run(`ok - succeed before timeout`, subtest(time.Second, 10*time.Millisecond, 50*time.Millisecond, ``))

	c.Run(`nok`, subtest(time.Second, 100*time.Millisecond, 2*time.Second, "^(nope\n){10}timeout$"))
}

func fixture(ttl time.Duration) func() error {
	t := time.After(ttl)
	return func() error {
		select {
		case <-t:
			return nil
		default:
			return errFixture
		}
	}
}
