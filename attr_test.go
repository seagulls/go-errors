package errors_test

import (
	"testing"

	"gitlab.com/seagulls/go-errors"
)

func TestAnnotatedError(t *testing.T) {
	ce := errors.Const(`const error`)
	err := errors.Attribute(ce, `foo`, 1, `bar`, "two")

	expect := `err(const error) {"foo":1,"bar":"two"}`
	actual := err.Error()
	if actual != expect {
		t.Errorf("expected %q, got %q\n", expect, actual)
		return
	}
}
