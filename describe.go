package errors

func Describe(e error, d string) error {
	er, ok := e.(err)
	if !ok {
		er := err{}
		if e != nil {
			er[KeyErr] = e
		}
	}

	er[KeyDesc] = d
	return er
}

func OptDescribe(d string) Option {
	return func(e error) error {
		et, ok := e.(err)
		if !ok {
			return e
		}

		et[KeyDesc] = d
		return et
	}
}
